Apache2
Apache 2
Baseado na imagem ubunto:latest
Building
•	Configuração do arquivo Dockerfile:
FROM ubuntu
MAINTAINER André Felippe afelipp3@gmail.com
RUN apt-get update && apt-get install -y apache2 curl && apt-get clean
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
CMD service apache2 start && /bin/bash
LABEL Description="Imagem contendo serviço apache2"
EXPOSE 80 
EXPOSE 443
WORKDIR /home
Exposed ports (Portas do serviço)
•	80
•	443
Comados utilizados
#Comando utilizado para construir a imagem localmente 

docker build -t yhanz1410/apache_pi:latest .

#Comando utilizado para enviar a imagem para o repositório docker (Após estar logado com o docker login)
	
Docker push yhanz1410/apache_pi:latest
	
