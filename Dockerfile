FROM ubuntu

MAINTAINER André Felippe afelipp3@gmail.com

RUN apt-get update && apt-get install -y apache2 && apt-get clean

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2

LABEL Description="Imagem contendo serviço apache2"

EXPOSE 80
EXPOSE 443

WORKDIR /home

CMD service apache2 start && /bin/bash
